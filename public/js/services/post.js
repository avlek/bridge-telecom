module.exports = ['$resource', function ($resource) {
  return $resource('api/posts/:id.json');
}];